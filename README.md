[![star](https://gitee.com/itsay/resume/badge/star.svg?theme=white)](https://gitee.com/itsay/resume/stargazers)    [![fork](https://gitee.com/itsay/resume/badge/fork.svg?theme=white)](https://gitee.com/itsay/resume/members)

# 个人简历模板（张大侠）

[http://itsay.gitee.io/resume](http://itsay.gitee.io/resume)

# 吴扬扬的个人简历

[http://nfunm082.gitee.io/resume](http://nfunm082.gitee.io/resume)

## Intro

参照张大侠的简历模板，用已学知识和自身实际修改样式和内容。

## Object

新媒体运营

## Preview

### PC端
![](assets/images/pc.png)

### 移动端
![](assets/images/ip.png)

## ChangeLog
- 2017.3.7 创建模板
- 2017.3.12 移动端优化
- 2017.12.20 修改部分内容

## Acknowledgments
- font-awesome提供字体图标

## LICENSE

MIT © [ITSAY](http://blog.if2er.com)